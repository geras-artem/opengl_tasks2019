#pragma once

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <random>


// класс хранит информацию о внутреннем стоении лабиринта
class Maze
{
public:
    // координаты стартовой позиции в лабиринте
    float startXPosition, startYPosition;
    // создать лабиринт на основе файла в формате
    //  h w - высота и ширина лабиринта соответсвенно
    //
    //  ##.##. картинка h*v где # - стенка . - пустота

    Maze(std::string filepath);

    std::vector<bool>& operator[](int idx);

    const std::vector<bool>& operator[](int idx) const;
    // высота лабиринта
    unsigned long GetHeight() const;
    // ширина лабиринта
    unsigned long GetWidth() const;

private:
    // клетки лабиринта
    std::vector<std::vector<bool> > mazeCells;
};
