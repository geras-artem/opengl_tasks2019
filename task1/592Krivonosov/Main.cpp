#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>
#include <algorithm>

glm::vec3 countNormal(float a, float u, float v) {
    glm::vec3 dev_u = glm::vec3(
            sin(u) * (sin(u / 2) * sin(2 * v) - cos(u / 2) * sin(v) - a) - cos(u) * (sin(u / 2) * sin(v) + cos(u / 2) * sin(2 * v)) / 2,
            cos(u) * (a + cos(u / 2) * sin(v) - sin(u / 2) * sin(2 * v)) - sin(u) * (sin(u / 2) * sin(v) + cos(u / 2) * sin(2 * v)) / 2,
            (cos(u / 2) * sin(v) - sin(u / 2) * sin(2 * v)) / 2);
    glm::vec3 dev_v = glm::vec3(
            cos(u) * (cos(u / 2) * cos(v) - 2 * sin(u / 2) * cos(2 * v)),
            sin(u) * (cos(u / 2) * cos(v) - 2 * sin(u / 2) * cos(2 * v)),
            2 * cos(u / 2) * cos(2 * v) + sin(u / 2) * cos(v));
    return glm::normalize(glm::cross(dev_u, dev_v));
}

MeshPtr makeKleinBottle(float a, float period) {

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    for (unsigned int i = 0; i < period; i++)
    {
        float u = 2 * glm::pi<float>() * i / period;
        float u1 = (i + 1) > period ? 2 * glm::pi<float>() : 2 * glm::pi<float>() * (i + 1) / period;

        for (unsigned int j = 0; j < period; j++)
        {
            float v = 2 * glm::pi<float>() * j / period;
            float v1 = (j + 1) > period ? 2 * glm::pi<float>() : 2 * glm::pi<float>() * (j + 1) / period;

            glm::vec3 vertex_u_v = glm::vec3(
                    (a + cos(u / 2) * sin(v) - sin(u / 2) * sin(2 * v)) * cos(u),
                    (a + cos(u / 2) * sin(v) - sin(u / 2) * sin(2 * v)) * sin(u),
                    sin(u / 2) * sin(v) + cos(u / 2) * sin(2 * v));
            glm::vec3 vertex_u_v1 = glm::vec3(
                    (a + cos(u / 2) * sin(v1) - sin(u / 2) * sin(2 * v1)) * cos(u),
                    (a + cos(u / 2) * sin(v1) - sin(u / 2) * sin(2 * v1)) * sin(u),
                    sin(u / 2) * sin(v1) + cos(u / 2) * sin(2 * v1));
            glm::vec3 vertex_u1_v = glm::vec3(
                    (a + cos(u1 / 2) * sin(v) - sin(u1 / 2) * sin(2 * v)) * cos(u1),
                    (a + cos(u1 / 2) * sin(v) - sin(u1 / 2) * sin(2 * v)) * sin(u1),
                    sin(u1 / 2) * sin(v) + cos(u1 / 2) * sin(2 * v));
            glm::vec3 vertex_u1_v1 = glm::vec3(
                    (a + cos(u1 / 2) * sin(v1) - sin(u1 / 2) * sin(2 * v1)) * cos(u1),
                    (a + cos(u1 / 2) * sin(v1) - sin(u1 / 2) * sin(2 * v1)) * sin(u1),
                    sin(u1 / 2) * sin(v1) + cos(u1 / 2) * sin(2 * v1));

            vertices.push_back(vertex_u_v);
            vertices.push_back(vertex_u_v1);
            vertices.push_back(vertex_u1_v);
            vertices.push_back(vertex_u_v1);
            vertices.push_back(vertex_u1_v);
            vertices.push_back(vertex_u1_v1);

            normals.push_back(countNormal(a, u, v));
            normals.push_back(countNormal(a, u, v1));
            normals.push_back(countNormal(a, u1, v));
            normals.push_back(countNormal(a, u, v1));
            normals.push_back(countNormal(a, u1, v));
            normals.push_back(countNormal(a, u1, v1));
        }
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}

class SampleApplication : public Application
{
public:
    MeshPtr _KleinBottle;

    ShaderProgramPtr _shader;

    float _period = 50.0f;

    float a = 3.0f;

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        _KleinBottle = makeKleinBottle(a, _period);

        _shader = std::make_shared<ShaderProgram>("592KrivonosovData1/shaderNormal.vert", "592KrivonosovData1/shader.frag");
    }

    void update() override
    {
        float dt = glfwGetTime() - _oldTime;

        if (glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS) {
            _period = std::max(4.0f, _period - 100 * dt);
            std::cout << _period << std::endl;
            _KleinBottle = makeKleinBottle(a, _period);
        }
        if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
            _period = std::min(100.0f, _period + 100 * dt);
            std::cout << _period << std::endl;
            _KleinBottle = makeKleinBottle(a, _period);
        }
        Application::update();
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader->use();

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        _shader->setMat4Uniform("modelMatrix", _KleinBottle->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix",
                                glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _KleinBottle->modelMatrix()))));
        _KleinBottle->draw();
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
