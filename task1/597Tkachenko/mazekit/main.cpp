#include "maze_application.hpp"


int main(int argc, char** argv)
{
    mazekit::MazeApplication mazeApplication;
    mazeApplication.start();
    return 0;
}