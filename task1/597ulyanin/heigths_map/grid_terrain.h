#pragma once

#include "heights_map.h"

#include <Mesh.hpp>

#include <glm/vec3.hpp>
#include <glm/glm.hpp>
#include <array>

class GridTerrain {
public:

    explicit GridTerrain(const HeightsMap& heightsMap) {
        LoadFromHeightsMap(heightsMap);
    }

    void LoadFromHeightsMap(const HeightsMap& heightsMap) {
        auto [height, width] = heightsMap.GetWidthHeight();
        Height_ = height;
        Width_ = width;
        CalcVertices(heightsMap);
        CalcNormals(heightsMap);
        assert(Normals_.size() == Vertices_.size());
        assert(!Normals_.empty());
    }

    MeshPtr GetMesh() const {
        MeshPtr mesh = std::make_shared<Mesh>();

        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;


        for (int i = 0; i + 1 < Height_; i++) {
            for (int j = 0; j + 1 < Width_; j++) {
                static constexpr size_t NumTriangles = 3;
                uint8_t TrianglesMoves[NumTriangles][3][2] = {
                    {
                        {0, 0},
                        {0, 1},
                        {1, 0},
                    },
                    {
                        {0, 1},
                        {1, 1},
                        {1, 0},
                    },
                };
                for (auto& TrianglesMove : TrianglesMoves) {
                    for (size_t q = 0; q < 3; ++q) {
                        glm::vec3 nextVertex;
                        glm::vec3 nextNormal;
                        int di = TrianglesMove[q][0];
                        int dj = TrianglesMove[q][1];
                        if (
                            GetNeighbour(i, j, di, dj, &nextVertex)
                            && GetNeighbour(i, j, di, dj, &nextNormal, NeighbourMode::Normal)
                        ) {
                            vertices.push_back(std::move(nextVertex));
                            normals.push_back(std::move(nextNormal));
                        }
                    }
                }
            }
        }

        assert(vertices.size() % 3 == 0);

        /////////////////////////////

        static_assert(sizeof(float) * 3 == sizeof(glm::vec3), "unexpected vec3 size");

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(glm::vec3), vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(glm::vec3), normals.data());


        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);

        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());


        return mesh;
    }

    float GetOpenGLHeight() const {
        return 1.f / Height_* Scale;
    }

    float GetOpenGLWidth() const {
        return 1.f / Width_* Scale;
    }


private:
    std::vector<glm::vec3> Vertices_;
    std::vector<glm::vec3> Normals_;
    size_t Height_ = 0;
    size_t Width_ = 0;
    float Scale = 1.0;

    // direction clockwise
    static constexpr std::array<std::pair<int8_t, int8_t>, 4> Move{{
        {1, 0}, // right
        {0, 1}, // up
        {-1, 0}, // left
        {0, -1}, // down
    }};

    void CalcVertices(const HeightsMap& heightsMap) {
        Vertices_.reserve(Height_ * Width_);
        for (size_t i = 0; i < Height_; ++i) {
            for (size_t j = 0; j < Width_; ++j) {
                Vertices_.emplace_back(
                    static_cast<float>(i) / Height_ * Scale,
                    static_cast<float>(j) / Width_ * Scale,
                    heightsMap.Heights[i][j]
                );
            }
        }
    }

    void CalcNormals(const HeightsMap& heightsMap) {
        auto [height, width] = heightsMap.GetWidthHeight();
        Normals_.reserve(height * width);
        for (size_t i = 0; i < height; ++i) {
            for (size_t j = 0; j < width; ++j) {
                glm::vec3 normalsSum;
                std::vector<glm::vec3> directions;
                const glm::vec3& v = Vertices_[GetIndex(i, j)];
                for (glm::vec3 to : GetNeighbours(i, j)) {
                    directions.push_back(to - v);
                }
                assert(directions.size() <= 4 && !directions.empty());
                directions.push_back(directions[0]);
                for (size_t t = 0; t + 1 < directions.size(); ++t) {
                    normalsSum += glm::cross(directions[i], directions[i + 1]);
                }
                Normals_.emplace_back(glm::normalize(normalsSum));
            }
        }
    }

    size_t GetIndex(size_t i, size_t j) const {
        assert(i * Width_ + j < Vertices_.size());
        return i * Width_ + j;
    }

    std::vector<glm::vec3> GetNeighbours(size_t i, size_t j) const {
        std::vector<glm::vec3> neighbours;
        for (size_t t = 0; t < 4; ++t) {
            glm::vec3 to;
            if (GetNeighbour(i, j, Move[t].first, Move[t].second, &to)) {
                neighbours.push_back(std::move(to));
            }
        }
        return neighbours;
    }

    enum class NeighbourMode {
        Vertex = 0,
        Normal = 1,
    };

    bool GetNeighbour(int i, int j, int di, int dj, glm::vec3* v, NeighbourMode mode = NeighbourMode::Vertex) const {
        int newI = i + di;
        int newJ = j + dj;
        if (newI < 0 || newI >= Height_ || newJ < 0 || newJ >= Width_) {
            return false;
        }
        if (mode == NeighbourMode::Vertex) {
            *v = Vertices_[GetIndex(newI, newJ)];
        } else {
            *v = Normals_[GetIndex(newI, newJ)];
        }
        return true;
    }
};
