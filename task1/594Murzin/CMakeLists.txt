add_compile_definitions(GLM_ENABLE_EXPERIMENTAL)

set(HEADER_FILES
        common/Application.hpp
        common/DebugOutput.h
        common/Camera.hpp
        common/Mesh.hpp
        common/ShaderProgram.hpp
        cylinder.h)

set(SRC_FILES
        common/Application.cpp
        common/DebugOutput.cpp
        common/Camera.cpp
        common/Mesh.cpp
        common/ShaderProgram.cpp
        main.cpp)

MAKE_OPENGL_TASK(594Murzin 1 "${SRC_FILES}")

if (UNIX)
    target_link_libraries(594Murzin1 stdc++fs)
endif ()
