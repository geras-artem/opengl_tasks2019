#version 450

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;

out vec4 color;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

void main()
{
    color.rgb = normal.xyz * 0.5 + 0.5;
    color.a = 1.0;

	gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(position, 1.0);
}
